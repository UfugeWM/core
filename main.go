package main

import (
	"os"
	"log"
	"flag"
	"os/exec"
	"strings"
	"io/ioutil"
	"path/filepath"

	"github.com/golang/protobuf/proto"
	"gitlab.com/UfugeWM/go/ufugewm_proto"
	"github.com/BurntSushi/xdg"
)

var paths = xdg.Paths{
	XDGSuffix: "ufugewm",
	GoImportPath: "gitlab.com/UfugeWM/core/config",
}

var (
	networkVar = flag.String("p", "unix", "The protocol plugins should use to extend the UfugeWM")
	addressVar = flag.String("a", "/var/run/ufugewm.sock", "The address plugins should use to extend UfugeWM")
)

func runConfig(network, address string) {
	file, err := paths.ConfigFile("config")
	if err != nil {return}
	cmd := exec.Command(file)
	cmd.Env = append(os.Environ(), "UFUGEWM_NETWORK="+network, "UFUGEWM_ADDRESS="+address)
	err = cmd.Start()
	if err != nil {
		log.Println(err)
	}
}

func main() {
	listener, err := net.Listen(*networkVar, *addressVar)
	if err != nil {
	    log.Fatal(err)
	}
	runConfig()
	buf := proto.NewBuffer()
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		b, err := ioutil.ReadAll(config)
		if err != nil {
			log.Println(err)
			continue
		}
		buf.SetBuf(b)
	}
}
